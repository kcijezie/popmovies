# Popular Movies App

Popular Movies App allows users to discover the most popular movies playing. The web app (optimized for mobile) has a Main Discovery Screen and a Details View as shown in the screenshots below:

![picture alt](/src/components/assets/images/screenshot.png 'Screenshots')

### Features

1. The main discovery screen presents the user with a grid arrangement of movie posters.
2. Clicking or tapping on a movie poster will display a details screen with additional information such as original title, movie poster image thumbnail, movie overview, rating and release date.

[<img src="/src/components/assets/images/videocover.png" width="50%">](/src/components/assets/images/walkthrough.mp4 'See Popmovies in action!')

## Running The App

Clone git repo:\
The source code can be found in the git repository - [https://gitlab.com/kcijezie/popmovies](https://gitlab.com/kcijezie/popmovies)

### `$ git clone https://gitlab.com/kcijezie/popmovies.git`

To view the files,\
Go to the new directory - popmovies

### `$ cd popmovies`

Once in the directory,<br /> Install dependencies by running:

### `yarn install`

**Add your themoviedb.org API key**<br />
Open `popmovies/src/components/common/hooks/useBranding.t` in a code editor of your choice and swap out the empty string value of `moviesApiKey` property with your own _API key_. Save the file. <br />

Run the app in the development mode.<br />

### `yarn start`

Open _http://localhost:3000_ to view it in the browser. <br />

![picture alt](/src/components/assets/images/desk_cropped.png 'Screenshots')

## Running Unit tests

This will launch the test runner in the interactive watch mode.<br />
Go to cloned project directory - _popmovies_ and run the following command in your terminal:

### `yarn test`

## To-do Features

1. Add movie to favourite list
2. Play movie trailers
