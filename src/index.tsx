import 'bootstrap/dist/css/bootstrap.css';
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import ErrorPage from './components/routes/error';
import MoviesContainer from './components/routes/movies';
import MovieContainer from './components/routes/movie';

const router = createBrowserRouter([
  {
    path: '/',
    element: <MoviesContainer />,
    errorElement: <ErrorPage />,
  },
  {
    path: '/:movieId',
    element: <MovieContainer />,
    errorElement: <ErrorPage />,
  },
]);

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(<RouterProvider router={router} />);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
