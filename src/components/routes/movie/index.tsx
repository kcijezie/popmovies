import { useNavigate, useParams } from 'react-router-dom';
import Navbar from '../../common/NavBar';
import { useEffect, useState } from 'react';
import { useBranding } from '../../common/hooks/useBranding';
import axios, { AxiosError } from 'axios';
import { AppendImageUrl } from '../../common/utils';
import moment from 'moment';
import styled from '@emotion/styled';
import { AiOutlinePlayCircle } from 'react-icons/ai';
import { MovieType, ThemeType } from '../../common/types';
import { ThemeProvider } from '@emotion/react';
import { Helmet } from 'react-helmet';
import React from 'react';

const StyledButton = styled.button<{
  theme?: ThemeType;
}>`
  color: #ffffff;
  background-color: ${(props) => props.theme?.secondary};
  border-radius: 0px;
  font-weight: 500;
  &:hover {
    background-color: ${(props) => props.theme?.primary};
    color: #fff;
  }
`;

const StyledPlayerDiv = styled.div<{
  theme?: ThemeType;
}>`
  background: ${(props) => props.theme?.light};
  padding: 18px;
`;

const StyledHeading = styled.div<{
  theme?: ThemeType;
}>`
  text-transform: uppercase;
  font-size: 14px;
  font-weight: 500;
  line-height: 24px;
  letter-spacing: 0.02em;
  text-align: left;
  border-bottom: 1px solid ${(props) => props.theme?.dark};
  padding-bottom: 4px;
`;

const StyledDate = styled.div`
  font-size: 20px;
  font-weight: 400;
  line-height: 24px;
  letter-spacing: 0em;
  text-align: left;
`;

const StyledRunTime = styled.div`
  font-size: 14px;
  font-style: italic;
  font-weight: 400;
  line-height: 24px;
  letter-spacing: 0em;
  text-align: left;
`;

const StyledRating = styled.div`
  font-size: 14px;
  font-weight: 700;
  line-height: 24px;
  letter-spacing: 0em;
  text-align: left;
`;
const Movie: React.FC = () => {
  const { moviesApiKey, moviesApi, imageUrl } = useBranding();
  const navigate = useNavigate();
  const { movieId } = useParams();
  const [movie, setMovie] = useState<MovieType | null>(null);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);

  useEffect(() => {
    const fetch = async () => {
      try {
        const { data } = await axios.get(
          `${moviesApi}/${movieId}?api_key=${moviesApiKey}`
        );

        const updateResults = AppendImageUrl({ payload: data, imageUrl });

        setMovie(updateResults);
      } catch (error) {
        const err = error as AxiosError;
        if (err.response) {
          const { status_message } = err.response?.data as {
            status_message: string;
          };
          setErrorMessage(status_message);
        } else {
          setErrorMessage('An API request error has occured!');
        }
      }
    };

    if (moviesApiKey && moviesApi && imageUrl) {
      fetch();
    }
  }, [movieId, moviesApi, imageUrl, moviesApiKey]);
  return (
    <div>
      <Helmet>
        <title>{movie?.original_title as string}</title>
      </Helmet>
      <Navbar
        title='Movie details'
        handleBackArrowClick={() => navigate('/')}
      />
      {!errorMessage && (
        <Navbar
          title={movie?.original_title as string}
          showMenu={false}
          variant='secondary'
        />
      )}

      <div className='container-md'>
        {errorMessage ? (
          <p className='pt-4'>{errorMessage}</p>
        ) : (
          <>
            <div className='row pt-4'>
              <div className='d-flex flex-row mb-3'>
                <div>
                  <img
                    src={movie?.poster_path as string}
                    alt='Movie poster'
                  />
                </div>
                <div>
                  <div className='d-flex flex-column h-100 ps-3'>
                    <StyledDate>
                      {moment(movie?.release_date as string).format('YYYY')}
                    </StyledDate>
                    <StyledRunTime>{`${
                      movie?.runtime as number
                    } mins`}</StyledRunTime>

                    <div className='mt-auto'>
                      <div className='d-flex flex-column'>
                        <StyledRating className='pb-4'>{`${(
                          movie?.vote_average as number
                        )?.toFixed(1)}/10`}</StyledRating>

                        <div>
                          <StyledButton
                            type='button'
                            className='btn w-100'
                          >
                            Add to Favourite
                          </StyledButton>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='row pt-3 pb-3'>
              <div className='col-12'>{movie?.overview as string}</div>
              <div className='col-12 pt-4'>
                <StyledHeading>Trailers</StyledHeading>
              </div>
              <div className='col-12 pt-3'>
                <StyledPlayerDiv className='d-flex flex-row align-items-center'>
                  <div className='me-3'>
                    <AiOutlinePlayCircle size={28} />
                  </div>
                  <div className=''>Play trailer 1</div>
                </StyledPlayerDiv>
              </div>
              <div className='col-12 pt-2'>
                <StyledPlayerDiv className='d-flex flex-row align-items-center'>
                  <div className='me-3'>
                    <AiOutlinePlayCircle size={28} />
                  </div>
                  <div className=''>Play trailer 2</div>
                </StyledPlayerDiv>
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

const MovieContainer = () => {
  const { colour } = useBranding();

  return (
    <ThemeProvider theme={colour}>
      <Movie />
    </ThemeProvider>
  );
};

export default MovieContainer;
