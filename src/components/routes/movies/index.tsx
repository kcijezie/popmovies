import { useNavigate } from 'react-router-dom';
import Navbar from '../../common/NavBar';
import { useEffect, useState } from 'react';
import axios, { AxiosError } from 'axios';
import { useBranding } from '../../common/hooks/useBranding';
import { AppendImageUrlToPayload } from '../../common/utils';
import styled from '@emotion/styled';
import { MovieType } from '../../common/types';
import { ThemeProvider } from '@emotion/react';
import { Helmet } from 'react-helmet';
import React from 'react';

const StyledImageDiv = styled.div`
  cursor: pointer;
  img {
    width: 100%;
    height: 100%;
  }
`;
const Movies: React.FC = () => {
  const { moviesApiKey, moviesApi, imageUrl } = useBranding();
  const [movies, setMovies] = useState<MovieType[] | null>(null);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const navigate = useNavigate();
  useEffect(() => {
    const fetch = async () => {
      try {
        const { data } = await axios.get(
          `${moviesApi}/popular?api_key=${moviesApiKey}`
        );
        const { results } = data;
        const updateResults = AppendImageUrlToPayload({
          payload: results,
          imageUrl,
        });
        setMovies(updateResults as MovieType[]);
        setErrorMessage(null);
      } catch (error) {
        const err = error as AxiosError;
        if (err.response) {
          const { status_message } = err.response?.data as {
            status_message: string;
          };
          setErrorMessage(status_message);
        } else {
          setErrorMessage('An API request error has occured!');
        }
      }
    };
    if (moviesApiKey && moviesApi && imageUrl) {
      fetch();
    }
  }, [moviesApiKey, moviesApi, imageUrl]);
  return (
    <div>
      <Helmet>
        <title>Popular Movies</title>
      </Helmet>
      <Navbar title='Popular Movies' />
      <div className='container-fluid'>
        <div className='row'>
          {errorMessage ? (
            <p className='pt-4'>{errorMessage} </p>
          ) : (
            <>
              {movies?.map(({ id, poster_path }) => {
                return (
                  <StyledImageDiv
                    key={id}
                    className='col-6 col-lg-1 col-md-2 p-0'
                    onClick={() => navigate(`/${id}`)}
                  >
                    <img
                      src={poster_path}
                      alt='Movie poster'
                    />
                  </StyledImageDiv>
                );
              })}
            </>
          )}
        </div>
      </div>
    </div>
  );
};

const MoviesContainer = () => {
  const { colour } = useBranding();

  return (
    <ThemeProvider theme={colour}>
      <Movies />
    </ThemeProvider>
  );
};

export default MoviesContainer;
