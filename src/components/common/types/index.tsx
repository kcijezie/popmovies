export type ThemeType = {
  primary: string;
  secondary: string;
  light: string;
  dark: string;
};
export type MovieType = {
  poster_path: string;
  original_title: string;
  release_date: string;
  runtime: number;
  vote_average: number;
  overview: string;
  id: number;
};
