export const useBranding = () => {
  return {
    colour: {
      primary: '#212121',
      secondary: '#746A64',
      light: '#fafafa',
      dark: '#dedede',
    },
    moviesApiKey: '',
    moviesApi: 'https://api.themoviedb.org/3/movie',
    imageUrl: 'https://image.tmdb.org/t/p/w185',
  };
};
