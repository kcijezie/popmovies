import _ from 'lodash';
import { MovieType } from '../types';

export const AppendImageUrlToPayload = ({
  payload,
  imageUrl,
}: {
  payload: MovieType[];
  imageUrl: string;
}) => {
  if (!_.isArray(payload) || !imageUrl) return null;
  const updatedPayload = payload.map((movie) => {
    return {
      ...movie,
      poster_path: `${imageUrl}${movie.poster_path}`,
    };
  });
  return updatedPayload;
};

export const AppendImageUrl = ({
  payload,
  imageUrl,
}: {
  payload: MovieType;
  imageUrl: string;
}) => {
  if (!_.isObject(payload) || !imageUrl) return null;
  const updatedPayload = {
    ...payload,
    poster_path: `${imageUrl}${payload.poster_path}`,
  };
  return updatedPayload;
};
