import React from 'react';
import { render, screen } from '@testing-library/react';
import App from '../../../App';
import { useBranding } from '../hooks/useBranding';
import { AppendImageUrl, AppendImageUrlToPayload } from '.';
// import App from './App';

const sampleData = {
  id: 575264,
  original_title: 'Mission: Impossible - Dead Reckoning Part One',
  overview:
    "Ethan Hunt and his IMF team embark on their most dangerous mission yet: To track down a terrifying new weapon that threatens all of humanity before it falls into the wrong hands. With control of the future and the world's fate at stake and dark forces from Ethan's past closing in, a deadly race around the globe begins. Confronted by a mysterious, all-powerful enemy, Ethan must consider that nothing can matter more than his mission—not even the lives of those he cares about most.",

  poster_path: '/NNxYkU70HPurnNCSiCjYAmacwm.jpg',

  release_date: '2023-07-08',

  runtime: 164,

  vote_average: 7.605,
};

test('Append Image Url with Path', () => {
  const { imageUrl } = useBranding();
  const updatedPayload = AppendImageUrl({ payload: sampleData, imageUrl });
  expect(updatedPayload).toEqual({
    id: 575264,
    original_title: 'Mission: Impossible - Dead Reckoning Part One',
    overview:
      "Ethan Hunt and his IMF team embark on their most dangerous mission yet: To track down a terrifying new weapon that threatens all of humanity before it falls into the wrong hands. With control of the future and the world's fate at stake and dark forces from Ethan's past closing in, a deadly race around the globe begins. Confronted by a mysterious, all-powerful enemy, Ethan must consider that nothing can matter more than his mission—not even the lives of those he cares about most.",
    poster_path: imageUrl + '/NNxYkU70HPurnNCSiCjYAmacwm.jpg',
    release_date: '2023-07-08',
    runtime: 164,
    vote_average: 7.605,
  });
});

test('Append Image Url with Path to all Movies', () => {
  const { imageUrl } = useBranding();
  const updatedPayload = AppendImageUrlToPayload({
    payload: [sampleData],
    imageUrl,
  });
  expect(updatedPayload).toEqual([
    {
      id: 575264,
      original_title: 'Mission: Impossible - Dead Reckoning Part One',
      overview:
        "Ethan Hunt and his IMF team embark on their most dangerous mission yet: To track down a terrifying new weapon that threatens all of humanity before it falls into the wrong hands. With control of the future and the world's fate at stake and dark forces from Ethan's past closing in, a deadly race around the globe begins. Confronted by a mysterious, all-powerful enemy, Ethan must consider that nothing can matter more than his mission—not even the lives of those he cares about most.",
      poster_path: imageUrl + '/NNxYkU70HPurnNCSiCjYAmacwm.jpg',
      release_date: '2023-07-08',
      runtime: 164,
      vote_average: 7.605,
    },
  ]);
});
