import styled from '@emotion/styled';
import React from 'react';
import { BsThreeDotsVertical } from 'react-icons/bs';
import { AiOutlineArrowLeft } from 'react-icons/ai';
import { ThemeType } from '../types';

const StyledNav = styled.nav<{
  variant: string;
  theme?: ThemeType;
}>`
  color: #ffffff;
  background: ${(props) =>
    props.variant === 'primary'
      ? props.theme?.primary
      : props.theme?.secondary};
  height: 64px;
  font-size: 20px;
  font-weight: 700;
  line-height: 24px;
  letter-spacing: 0em;
  text-align: left;
`;
const StyledNavButtonDiv = styled.div`
  cursor: pointer;
`;

const Navbar = ({
  title,
  handleBackArrowClick,
  showMenu = true,
  variant = 'primary',
}: {
  title: string;
  handleBackArrowClick?: () => void;
  showMenu?: boolean;
  variant?: string;
}) => {
  return (
    <StyledNav
      className='navbar navbar-expand-lg'
      variant={variant}
    >
      <div className='container-fluid'>
        <div className='d-flex justify-content-between align-items-center'>
          {handleBackArrowClick && (
            <StyledNavButtonDiv
              onClick={handleBackArrowClick}
              className='me-2'
            >
              <AiOutlineArrowLeft size={24} />
            </StyledNavButtonDiv>
          )}

          {title}
        </div>

        {showMenu && (
          <StyledNavButtonDiv>
            <BsThreeDotsVertical />
          </StyledNavButtonDiv>
        )}
      </div>
    </StyledNav>
  );
};

export default Navbar;
